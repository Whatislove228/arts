@extends('layouts.app_front')

@section('content')
    <div class="products-catagories-area clearfix">
        <div class="amado-pro-catagory clearfix">

            @foreach($taxonomies as $taxonomy)
            <div class="single-products-catagory clearfix">
                <a href="{{route('product.categories',['taxonomyName' => $taxonomy->slug])}}">
                    <img src="{{$taxonomy->getMedia()->first() ? $taxonomy->getMedia()->first()->getUrl('thumbnail') : 'img/bg-img/1.jpg';}}" alt="">
                    <!-- Hover Content -->
                    <div class="hover-content">
                        <div class="line"></div>
                        <p>From $180</p>
                        <h4>{{$taxonomy->name}}</h4>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
{{--    <!-- ##### jQuery (Necessary for All JavaScript Plugins) ##### -->--}}
{{--    <script src="js/jquery/jquery-2.2.4.min.js"></script>--}}
{{--    <!-- Popper js -->--}}
{{--    <script src="js/popper.min.js"></script>--}}
{{--    <!-- Bootstrap js -->--}}
{{--    <script src="js/bootstrap.min.js"></script>--}}
{{--    <!-- Plugins js -->--}}
{{--    <script src="js/plugins.js"></script>--}}
{{--    <!-- Active js -->--}}
{{--    <script src="js/active.js"></script>--}}
@endsection
