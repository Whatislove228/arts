<?php

namespace App;

class Taxon extends \Vanilo\Category\Models\Taxon
{
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
