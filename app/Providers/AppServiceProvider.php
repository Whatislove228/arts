<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Schema;
use \Konekt\Gears\Defaults\SimpleSetting;
use \Vanilo\Category\Models\Taxon as Taxon;
use Vanilo\Category\Models\Taxonomy as Taxonomy;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        $this->app->concord->registerModel(\Konekt\User\Contracts\User::class, \App\User::class);
//        $this->app->concord->registerModel(\Vanilo\Category\Models\Taxon::class, \App\Taxon::class);
//        $this->app->concord->registerModel(\Vanilo\Category\Models\Taxonomy::class, \App\Taxonomy::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
