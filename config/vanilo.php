<?php

return [
    'order' => [
        'number' => [
            'generator' => 'nano_id',
        ]
    ],
    'framework' => [
        'image' => [
            'taxonomy' => [
                'variants' => [
                    'medium' => [
                        'width'  => 500,
                        'height' => 500,
                        'fit'    => 'crop'
                    ],
                    'banner' => [
                        'width'  => 1248,
                        'height' => 702,
                        'fit'    => 'crop'
                    ]
                ]
            ]
        ]
    ]
];
