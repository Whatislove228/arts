<?php

return [
    'modules' => [
        Konekt\AppShell\Providers\ModuleServiceProvider::class => [
            'ui' => [
                'name' => 'Vanilo',
                'url' => '/admin/product'
            ]
        ],
        Vanilo\Framework\Providers\ModuleServiceProvider::class => [
            'image' => [
                    'variants' => [
                        'thumbnail' => [
                            'width'  => 500,
                            'height' => 500,
                            'fit' => 'max'
                        ],
                        'medium' => [
                            'width'  => 800,
                            'height' => 800,
                            'fit' => 'max'
                        ]
                    ],
                    'taxonomy' => [
                        'thumbnail' => [
                            'width'  => 500,
                            'height' => 500,
                            'fit' => 'max'
                        ],
                        'medium' => [
                            'width'  => 800,
                            'height' => 800,
                            'fit' => 'max'
                        ]
                    ],
//                'products' => [
//                    'variants' => [
//                        'thumbnail' => [
//                            'width'  => 500,
//                            'height' => 500,
//                            'fit' => 'max'
//                        ],
//                        'medium' => [
//                            'width'  => 800,
//                            'height' => 800,
//                            'fit' => 'max'
//                        ]
//                    ]
//                ]
            ],
            'currency'  => [
//                'code'   => 'USD',
//                'sign'   => '$',
//                'format' => '%2$s%1$g' // see sprintf. Amount is the first argument, currency is the second
                'code'   => 'EUR',
                'sign'   => '€',
                'format' => '%1$g%2$s'
            ]
        ]
    ]
];
