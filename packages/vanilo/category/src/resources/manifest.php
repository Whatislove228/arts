<?php

declare(strict_types=1);

return [
    'name' => 'Vanilo Category Module',
    'version' => '2.2.0'
];
