<?php

declare(strict_types=1);

return [
    'name' => 'Vanilo Order Module',
    'version' => '2.2.1'
];
